<?php
/**
 * @file
 * cod_session_video.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cod_session_video_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_session_video.
  $permissions['create field_session_video'] = array(
    'name' => 'create field_session_video',
    'roles' => array(
      0 => 'administrator',
      1 => 'video archiving team',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_session_video.
  $permissions['edit field_session_video'] = array(
    'name' => 'edit field_session_video',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_session_video.
  $permissions['edit own field_session_video'] = array(
    'name' => 'edit own field_session_video',
    'roles' => array(
      0 => 'administrator',
      1 => 'video archiving team',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_session_video.
  $permissions['view field_session_video'] = array(
    'name' => 'view field_session_video',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_session_video.
  $permissions['view own field_session_video'] = array(
    'name' => 'view own field_session_video',
    'roles' => array(
      0 => 'administrator',
      1 => 'video archiving team',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
