<?php
/**
 * @file
 * cod_session.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function cod_session_filter_default_formats() {
  $formats = array();

  // Exported format: raw video.
  $formats['raw_video'] = array(
    'format' => 'raw_video',
    'name' => 'raw video',
    'cache' => '1',
    'status' => '1',
    'weight' => '-6',
    'filters' => array(),
  );

  return $formats;
}
